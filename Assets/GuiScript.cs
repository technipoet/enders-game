﻿using UnityEngine;
using System.Collections;

public class GuiScript : MonoBehaviour {
    public Texture background;
    public Texture bar;
    Rect backgroundRect;
    Rect barRect, groundedRect;
    float screenWidth, screenHeight, hBarOffset, wBarOffset, jumpPower;
    bool grounded;
	// Use this for initialization
	void Start () {
        screenWidth = Screen.width;
        screenHeight = Screen.height;
        jumpPower = 1;
        backgroundRect = new Rect(screenWidth / 3, screenHeight / 6 * 5, screenWidth / 3, screenHeight / 10);
        grounded = false;
        
	}
	
	// Update is called once per frame
	void Update () {
        screenWidth = Screen.width;
        screenHeight = Screen.height;
        hBarOffset = screenHeight / 13;
        
        backgroundRect = new Rect(Pcnt(33.3f, screenWidth), Pcnt(80f, screenHeight), Pcnt(33.3f, screenWidth), Pcnt(5f, screenHeight));
        barRect = new Rect(Pcnt(33.8f, screenWidth), Pcnt(80.5f, screenHeight), Pcnt(Pcnt(jumpPower, 32.3f), screenWidth), Pcnt(4f, screenHeight));
        groundedRect = new Rect(Pcnt(25f, screenWidth), Pcnt(80f, screenHeight), Pcnt(10f, screenWidth), Pcnt(10f, screenHeight));
        //backgroundRect = new Rect(60, 60, 60, 60);
        
	}

    void OnGUI()
    {
        if (grounded)
        {
            GUI.Button(groundedRect, "GROUNDED");
        }
        GUI.DrawTexture(backgroundRect, background);
        GUI.DrawTexture(barRect, bar);
        GUI.Label(backgroundRect, "JUMP");
        
    }

    float Pcnt(float perc, float whole)
    {
        return perc * (whole / 100);
    }

    public void SetJump(float amt) {
        if (amt >= 100) {
            jumpPower = 100;
        }
        else if (amt <= 1) {
            jumpPower = 1;
        }
        else {
            jumpPower = amt;
        }
    }

    public void SetGrounded(bool g)
    {
        grounded = g;
    }
}
