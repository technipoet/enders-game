﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {
    public ParticleSystem p;
	// Use this for initialization
	void Awake () {
        p.Pause();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision col)
    {
        p.Play();
        this.gameObject.SetActive(false);

    }
}
