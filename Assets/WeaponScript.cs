﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour {
    public GameObject crosshair;
    public Camera cam;
    public GameObject bullet;
    public float bulletSpeed;
    ArrayList ammo;
    public int MAX_AMMO;
    Vector3 crossPos;
    int ammoCount = 0;
    
	// Use this for initialization
	void Start () {
        crossPos = cam.transform.position + cam.transform.forward;
        ammo = new ArrayList();
        
        
        for (int i = 0; i < MAX_AMMO; i++)
        {
            Object newBullet = Instantiate(bullet, new Vector3(0, 0, 0), Quaternion.identity);
            Debug.Log(newBullet.GetType().ToString());
            ammo.Add(newBullet);
        }
	}
	
	// Update is called once per frame
	void Update () {
        crossPos = this.transform.position + cam.transform.forward;
        if (Input.GetButtonUp("Fire1"))
        {
            ((GameObject)ammo[ammoCount]).transform.position = crossPos;
            ((GameObject)ammo[ammoCount]).SetActive(true);
            ((GameObject)ammo[ammoCount]).rigidbody.AddForce(cam.transform.forward * bulletSpeed);
            if (ammoCount >= MAX_AMMO - 2)
            {
                ammoCount = 0;
            }
            else
            {
                ammoCount++;
            }
        }
	}
}
